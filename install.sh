#!/bin/bash

# Find all directories, excluding hidden ones and for each, copy all files from this directory to ~/.neverball/
find . -maxdepth 1 -type d ! -name ".*" -exec bash -c 'echo "-> `basename {}`"; cp -r {}/* ~/.neverball/' \;
