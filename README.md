Never**ball** courses, from discontinued neverforum.com, thanks to the Internet archive https://web.archive.org/web/20220103181016/http://neverforum.com/fmpbo/levels.php

This should be [here](https://github.com/Neverball/packages), in a `neverputt` subdir in official Neverball repo, but this is not the case for now so I put them here (temporary I hope)

cf: https://discord.com/channels/669254822758711297/759998468441112616/1173766017999183972

## How to install courses ?

Beware that (in version 1.6.0) you can only have a total of *16 levels* maximum. 9 of those levels are game default levels so you can't remove them. This gives place for **7 custom levels max**.

Each course is composed of 3 elements (`*` means: can be anything):
- a `holes-*.txt` that lists:
    - the screenshot file (jpg and png files works)
    - the name and description of the course
    - the holes files (generally in a sub-dir)
- the holes dir, typically `map-*` containing, for each hole:
    - `*.sol` files
    - `*.map` files
- the screenshot file, as referenced in `holes-*.txt`

### Linux install

To install a course, place `holes-*.txt` file and the holes dir (`map-*`), and all its files, in your `~/.neverball/` dir.

Example: to add 'jumpcourse' to your Neverputt courses:
- copy `jumpcourse/holes-jumpcourse.txt` to `~/.neverball`
- copy `jumpcourse/map-jumpcourse/` dir to `~/.neverball`

So you'll have:
```
`~/.neverball/
  |-- holes-jumpcourse.txt
  `-- map-jumpcourse
      |-- 01_drawbridge.map
      |-- 01_drawbridge.sol

      [....]

      |-- 19_conveyor.map
      |-- 19_conveyor.sol
      `-- jumpcourse.jpg
```

#### Automatic install of all courses

```bash
./install.sh
```

### Windows install

Windows installation is the same as Linux installation, except that you'll have to replace `~/.neverball` dir by `Documents\My Games\Neverball`
